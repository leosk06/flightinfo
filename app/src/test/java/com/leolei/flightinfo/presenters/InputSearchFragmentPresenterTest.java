package com.leolei.flightinfo.presenters;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import android.os.Bundle;

import com.leolei.flightinfo.BuildConfig;
import com.leolei.flightinfo.presenters.InputSearchFragmentPresenter.IView;

/**
 * Created by leolei on 2016/2/14.
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class InputSearchFragmentPresenterTest {
    private IView someIView;
    private InputSearchFragmentPresenter presenterInstance;
    private Bundle rotationBundle;

    // Random date the user might choose
    private final int someYear = 2005;
    private final int someMonth = 7;
    private final int someDay = 10;

    // Date returned by presenterInstance
    private int returnYear, returnMonth, returnDay;

    @Before
    public void setUp() {
        someIView = mock(IView.class);
        rotationBundle = new Bundle();
    }

    @Test
    public void shouldReturnProperDateWhenUserPicksDate() {
        givenPresenterIsCreatedForIView(someIView);
        givenPresenterIsInitializedWithNoBundle();
        givenNewDateFromDatePickerDialog(someYear, someMonth, someDay);
        whenQueryingPresenterForDate();
        thenDatesShouldBeTheSame();
    }

    @Test
    public void shouldReturnProperDateWhenDeviceIsRotated() {
        givenPresenterIsCreatedForIView(someIView);
        givenPresenterIsInitializedWithNoBundle();
        whenQueryingPresenterForDate();
        whenIViewIsRotated();
        thenPresenterShouldKeepTheSameDateAsBefore();

    }

    @Test
    public void shouldSetCarrierModeFieldErrorStateIfUserEntersEmptyString() {
        givenPresenterIsCreatedForIView(someIView);
        givenPresenterIsInitializedWithNoBundle();
        whenUserEntersCarrierMode("");
        thenCarrierModeFieldErrorStateShouldBe(true);
    }

    @Test
    public void shouldResetCarrierModeFieldErrorStateIfUserEntersNonEmptyString() {
        givenPresenterIsCreatedForIView(someIView);
        givenPresenterIsInitializedWithNoBundle();
        whenUserEntersCarrierMode("non-empty String");
        thenCarrierModeFieldErrorStateShouldBe(false);
    }

    @Test
    public void shouldSetFlightNumberFieldErrorStateIfUserEntersEmptyString() {
        givenPresenterIsCreatedForIView(someIView);
        givenPresenterIsInitializedWithNoBundle();
        whenUserEntersFlightNumber("");
        thenFlightNumberModeFieldErrorStateShouldBe(true);
    }

    @Test
    public void shouldResetFlightNumberFieldErrorStateIfUserEntersNonEmptyString() {
        givenPresenterIsCreatedForIView(someIView);
        givenPresenterIsInitializedWithNoBundle();
        whenUserEntersFlightNumber("non-empty String");
        thenFlightNumberModeFieldErrorStateShouldBe(false);
    }

    private void givenPresenterIsCreatedForIView(IView someIView) {
        presenterInstance = new InputSearchFragmentPresenter(someIView);
    }

    private void givenPresenterIsInitializedWithNoBundle() {
        givenPresenterIsInitializedWithBundle(null);
    }

    private void givenPresenterIsInitializedWithBundle(Bundle bundle) {
        presenterInstance.onCreate(bundle);
        presenterInstance.onCreateView(bundle);
        presenterInstance.onStart();
        presenterInstance.onResume();
    }

    private void givenNewDateFromDatePickerDialog(int year, int month, int day) {
        presenterInstance.onDateSet(year, month, day);
    }

    private void whenQueryingPresenterForDate() {
        returnYear = presenterInstance.getYear();
        returnMonth = presenterInstance.getMonth();
        returnDay = presenterInstance.getDay();
    }

    private void thenDatesShouldBeTheSame() {
        assertEquals(someYear, returnYear);
        assertEquals(someMonth, returnMonth);
        assertEquals(someDay, returnDay);
    }

    private void whenIViewIsRotated() {
        // Destroy current incarnation
        presenterInstance.onSaveInstanceState(rotationBundle);
        presenterInstance.onPause();
        presenterInstance.onStop();
        presenterInstance.onDestroy();

        // Create new incarnation
        someIView = mock(IView.class);
        presenterInstance = new InputSearchFragmentPresenter(someIView);
        presenterInstance.onCreate(rotationBundle);
        presenterInstance.onCreateView(rotationBundle);
        presenterInstance.onStart();
        presenterInstance.onResume();
    }

    private void thenPresenterShouldKeepTheSameDateAsBefore() {
        assertEquals(presenterInstance.getYear(), returnYear);
        assertEquals(presenterInstance.getMonth(), returnMonth);
        assertEquals(presenterInstance.getDay(), returnDay);
    }

    private void whenUserEntersCarrierMode(String newCarrierMode) {
        presenterInstance.onCarrierCodeChanged(newCarrierMode);
    }

    private void thenCarrierModeFieldErrorStateShouldBe(boolean newState) {
        verify(someIView).setCarrierCodeErrorState(newState);
    }

    private void whenUserEntersFlightNumber(String newCarrierMode) {
        presenterInstance.onFlightNumberChanged(newCarrierMode);
    }

    private void thenFlightNumberModeFieldErrorStateShouldBe(boolean newState) {
        verify(someIView).setFlightNumberErrorState(newState);
    }

}
