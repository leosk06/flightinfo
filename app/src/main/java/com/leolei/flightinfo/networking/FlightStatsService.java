package com.leolei.flightinfo.networking;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by leolei on 2016/2/15.
 */

public interface FlightStatsService {
    @GET("/flex/flightstatus/rest/v2/json/flight/status/{carrierCode}/{flightNo}/dep/{year}/{month}/{day}?utc=true")
    Call<FlightStatsApiResponse> getFlightInfoByDepartureDate(@Path("carrierCode") String carrierCode,
            @Path("flightNo") String flightNo, @Path("year") int year, @Path("month") int month,
            @Path("day") int day, @Query("appId") String appId, @Query("appKey") String appKey);
}
