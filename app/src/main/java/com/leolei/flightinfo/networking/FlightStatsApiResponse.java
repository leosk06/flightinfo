package com.leolei.flightinfo.networking;

import java.util.List;

import com.leolei.flightinfo.model.Appendix;
import com.leolei.flightinfo.model.ErrorMessage;
import com.leolei.flightinfo.model.FlightStatus;

/**
 * Created by leolei on 2016/2/15.
 */

public class FlightStatsApiResponse {
    private long timeStamp;
    private Appendix appendix;
    private List<FlightStatus> flightStatuses;
    private ErrorMessage error;

    public FlightStatsApiResponse() {
        timeStamp = System.currentTimeMillis();
    }

    public Appendix getAppendix() {
        return appendix;
    }

    public List<FlightStatus> getFlightStatuses() {
        return flightStatuses;
    }

    public ErrorMessage getError() {
        return error;
    }

    public long getTimeStamp() {
        return timeStamp;
    }
}
