package com.leolei.flightinfo.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leolei.flightinfo.Application;
import com.leolei.flightinfo.R;
import com.leolei.flightinfo.adapters.FlightStatusAdapter;
import com.leolei.flightinfo.presenters.FlightListFragmentPresenter;

/**
 * Created by leolei on 2016/2/15.
 */

public class FlightListFragment extends Fragment implements FlightListFragmentPresenter.IView {

    private FlightListFragmentPresenter presenter;
    private RecyclerView recyclerView;
    private FlightStatusAdapter adapter;
    private TextView errorMessage;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new FlightListFragmentPresenter(this);
        presenter.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_flight_list, container, false);

        errorMessage = (TextView) result.findViewById(R.id.error_message);

        presenter.onCreateView(savedInstanceState);
        return result;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
        setupSwipeRefresh();
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(Application.getInstance());

        adapter = new FlightStatusAdapter();

        //noinspection ConstantConditions // getView() won't return null since this is executed in onViewCreated
        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void setupSwipeRefresh() {
        //noinspection ConstantConditions // getView() won't return null since this is executed in onViewCreated
        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onSwipeToRefresh();
            }
        });
    }

    public void refreshList() {
        recyclerView.setVisibility(View.VISIBLE);
        errorMessage.setVisibility(View.GONE);
        adapter.refresh();
        hideProgressIndicator();
    }

    public void showErrorMessage(String error) {
        recyclerView.setVisibility(View.GONE);
        errorMessage.setVisibility(View.VISIBLE);
        errorMessage.setText(error);
    }

    @Override
    public void hideProgressIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showProgressIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }
}
