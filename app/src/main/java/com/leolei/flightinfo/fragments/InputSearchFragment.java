package com.leolei.flightinfo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.material.datetimepicker.date.DatePickerDialog;
import com.leolei.flightinfo.R;
import com.leolei.flightinfo.presenters.InputSearchFragmentPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * A placeholder fragment containing a simple view.
 */
public class InputSearchFragment extends Fragment implements InputSearchFragmentPresenter.IView {
    private static final String DATE_PICKER_TAG = "datePicker";

    private MaterialEditText etCarrierCode;
    private MaterialEditText etDate;
    private MaterialEditText etFlightNumber;

    private InputSearchFragmentPresenter presenter;
    private SearchRequestedListener searchRequestedListener;

    public InputSearchFragment() {
        super();
        presenter = new InputSearchFragmentPresenter(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_input_search, container, false);

        etDate = (MaterialEditText) result.findViewById(R.id.flight_date);
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        etCarrierCode = (MaterialEditText) result.findViewById(R.id.carrier_code);
        etCarrierCode.addTextChangedListener(new ConvenienceTextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                // Use this TextWatcher to pass code to uppercase
                // because android:textAllCaps is for API 12+ and I want to support API 10+
                String currentText = etCarrierCode.getText().toString();
                String currentTextUppercase = currentText.toUpperCase();
                if (!currentText.equals(currentTextUppercase)) {
                    s.clear();
                    s.append(currentTextUppercase);
                }
                presenter.onCarrierCodeChanged(currentTextUppercase);
            }
        });

        etFlightNumber = (MaterialEditText) result.findViewById(R.id.flight_number);
        etFlightNumber.addTextChangedListener(new ConvenienceTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                presenter.onFlightNumberChanged(s.toString());
            }
        });

        Button btSearch = (Button) result.findViewById(R.id.search);
        btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onSearchClicked();
            }
        });

        presenter.onCreateView(savedInstanceState);

        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    private void showDatePicker() {
        DatePickerDialog datePicker = new DatePickerDialog();
        datePicker.initialize(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                presenter.onDateSet(year, monthOfYear, dayOfMonth);
                dialog.dismiss();
            }
        }, presenter.getYear(), presenter.getMonth(), presenter.getDay());
        datePicker.show(getFragmentManager(), DATE_PICKER_TAG);
    }

    @Override
    public void setDate(String date) {
        etDate.setText(date);
    }

    @Override
    public void setCarrierCodeErrorState(boolean errorState) {
        setEditTextCodeErrorState(etCarrierCode, errorState);
    }

    @Override
    public void setFlightNumberErrorState(boolean errorState) {
        setEditTextCodeErrorState(etFlightNumber, errorState);
    }

    @Override
    public void showInvalidInputErrorMessage() {
        Toast.makeText(getActivity(), getString(R.string.invalid_input), Toast.LENGTH_LONG).show();
    }

    private void setEditTextCodeErrorState(MaterialEditText et, boolean errorState) {
        if (errorState) {
            et.setError(getString(R.string.cannot_be_empty));
        } else {
            et.setError(null);
        }
    }

    private static class ConvenienceTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    public interface SearchRequestedListener {
        void onSearchRequested(String carrierCode, String flightNumber, long dateInMillis);
    }

    public void setSearchRequestedListener(SearchRequestedListener searchRequestedListener) {
        this.searchRequestedListener = searchRequestedListener;
    }

    @Override
    public void notifySearchClicked(String carrierCode, String flightNumber, long dateInMillis) {
        searchRequestedListener.onSearchRequested(carrierCode, flightNumber, dateInMillis);
    }

}
