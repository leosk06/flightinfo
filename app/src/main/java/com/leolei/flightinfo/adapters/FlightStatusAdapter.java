package com.leolei.flightinfo.adapters;

/**
 * Created by leolei on 2016/2/16.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leolei.flightinfo.R;
import com.leolei.flightinfo.common.Utils;
import com.leolei.flightinfo.model.FlightStatus;
import com.leolei.flightinfo.networking.FlightStatsApiResponse;
import com.leolei.flightinfo.repositories.FlightDataRepository;

import java.util.List;

public class FlightStatusAdapter extends RecyclerView.Adapter<FlightStatusAdapter.ViewHolder> {
    private List<FlightStatus> data;

    public FlightStatusAdapter() {
        refreshData();
    }

    public void refresh() {
        refreshData();
        notifyDataSetChanged();
    }

    private void refreshData() {
        FlightStatsApiResponse response = FlightDataRepository.getInstance().getLastApiResponse();
        data = response == null? null : response.getFlightStatuses();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_flight_status_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        FlightStatus item = data.get(position);

        vh.title.setText(item.getDepartureAirportFsCode() + " - " + item.getArrivalAirportFsCode());
        vh.departureAirport.setText(item.getDepartureAirport().getName());
        vh.departureCountry.setText(item.getDepartureAirport().getCountryName());
        vh.departureCity.setText(item.getDepartureAirport().getCityName());
        vh.departureDateTime
                .setText(Utils.networkFormatDateToLocalizedDate(item.getDepartureDate().getDateLocal()));
        vh.arrivalAirport.setText(item.getArrivalAirport().getName());
        vh.arrivalCountry.setText(item.getArrivalAirport().getCountryName());
        vh.arrivalCity.setText(item.getArrivalAirport().getCityName());
        vh.arrivalDateTime.setText(Utils.networkFormatDateToLocalizedDate(item.getArrivalDate().getDateLocal()));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView departureAirport;
        public TextView arrivalAirport;
        public TextView departureCountry;
        public TextView arrivalCountry;
        public TextView departureDateTime;
        public TextView arrivalDateTime;
        public TextView departureCity;
        public TextView arrivalCity;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            departureAirport = (TextView) view.findViewById(R.id.departure_airport);
            arrivalAirport = (TextView) view.findViewById(R.id.arrival_airport);
            departureCountry = (TextView) view.findViewById(R.id.departure_country);
            arrivalCountry = (TextView) view.findViewById(R.id.arrival_country);
            departureDateTime = (TextView) view.findViewById(R.id.departure_datetime);
            arrivalDateTime = (TextView) view.findViewById(R.id.arrival_datetime);
            departureCity = (TextView) view.findViewById(R.id.departure_city);
            arrivalCity = (TextView) view.findViewById(R.id.arrival_city);
        }
    }
}
