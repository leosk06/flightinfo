package com.leolei.flightinfo;

import android.os.StrictMode;

/**
 * Created by leolei on 2016/2/14.
 */

public class Application extends android.app.Application {
    private static Application instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDeath().build());
        }
    }

    /**
     * Convenience pseudo-singleton to have global access to the Application context
     * even from objects that have no access to Android UI component objects.
     *
     * @return the global Application object
     */
    public static Application getInstance() {
        return instance;
    }

}
