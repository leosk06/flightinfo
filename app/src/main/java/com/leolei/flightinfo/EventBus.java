package com.leolei.flightinfo;

import com.squareup.otto.Bus;

/**
 * Singleton for enabling universal access to our event bus
 *
 * Created by leolei on 2016/2/16.
 */

public class EventBus {
    private static Bus instance;

    public static Bus getInstance() {
        if (instance == null) {
            instance = new Bus();
        }
        return instance;
    }
}
