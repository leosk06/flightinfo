package com.leolei.flightinfo.presenters;

import java.lang.ref.WeakReference;

import android.os.Bundle;

import com.leolei.flightinfo.repositories.FlightDataRepository;

/**
 * Created by leolei on 2016/2/16.
 */

public class FlightListFragmentPresenter extends Presenter {

    private WeakReference<IView> viewReference;

    public FlightListFragmentPresenter(IView view) {
        this.viewReference = new WeakReference<>(view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

        }
    }

    public void onSwipeToRefresh() {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        if (!FlightDataRepository.getInstance().repeatLastFlightStatusRequest()) {
            view.hideProgressIndicator();
        }
    }

    public interface IView {
        void hideProgressIndicator();

        void showProgressIndicator();
    }
}
