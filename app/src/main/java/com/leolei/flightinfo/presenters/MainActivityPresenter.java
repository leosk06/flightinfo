package com.leolei.flightinfo.presenters;

import java.lang.ref.WeakReference;

import android.os.Bundle;

import com.leolei.flightinfo.Application;
import com.leolei.flightinfo.EventBus;
import com.leolei.flightinfo.R;
import com.leolei.flightinfo.fragments.InputSearchFragment;
import com.leolei.flightinfo.model.ErrorMessage;
import com.leolei.flightinfo.networking.FlightStatsApiResponse;
import com.leolei.flightinfo.repositories.FlightDataRepository;
import com.squareup.otto.Subscribe;

/**
 * Created by leolei on 2016/2/14.
 */

public class MainActivityPresenter extends Presenter implements InputSearchFragment.SearchRequestedListener {
    private static final java.lang.String IS_PANEL_EXPANDED = "isPanelExpanded";

    private final WeakReference<IView> viewReference;

    private boolean isPanelExpanded = true;

    public MainActivityPresenter(IView view) {
        viewReference = new WeakReference<>(view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            isPanelExpanded = savedInstanceState.getBoolean(IS_PANEL_EXPANDED, isPanelExpanded);
        }
    }

    @Override
    public void onStart() {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }
        if (isPanelExpanded) {
            view.showSlidingPanel();
        } else {
            view.hideSlidingPanel();
        }
    }

    @Override
    public void onResume() {
        EventBus.getInstance().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getInstance().unregister(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_PANEL_EXPANDED, isPanelExpanded);
    }

    @Override
    public void onSearchRequested(String carrierCode, String flightNumber, long dateInMillis) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        view.hideSlidingPanel();
        view.showProgress();

        FlightDataRepository.getInstance().runFlightStatusRequest(carrierCode, flightNumber, dateInMillis);
    }

    /**
     * Subscribe to application-level problems (i.e., error messages from the webservice)
     */
    @Subscribe
    public void onFlightInfoError(ErrorMessage error) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }
        view.showErrorMessage(error.getErrorMessage());
    }

    /**
     * Subscribe to actual flight data (i.e., what the user is looking for)
     */
    @Subscribe
    public void onFlightInfoArrived(FlightStatsApiResponse info) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        view.hideSlidingPanel();

        if (info == null || info.getFlightStatuses().isEmpty()) {
            view.showErrorMessage(Application.getInstance().getString(R.string.no_results_found));
        } else {
            ErrorMessage errorMessage = info.getError();
            if (info.getError() != null) {
                view.showErrorMessage(info.getError().getErrorMessage());
                return;
            }

            view.refreshList();
        }
    }

    /**
     * Subscribe to network connectivity errors
     *
     * @param t
     */
    @Subscribe
    public void onFlightInfoRequestFailed(Throwable t) {
        // Netowork-level problems
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        view.hideSlidingPanel();
        view.showErrorMessage(t.getMessage());
    }

    public interface IView {

        void hideSlidingPanel();

        void showSlidingPanel();

        void refreshList();

        void showErrorMessage(String errorMessage);

        void showProgress();
    }
}
