package com.leolei.flightinfo.presenters;

import android.os.Bundle;

/**
 * Created by leolei on 2016/2/14.
 */

public abstract class Presenter {
    public void onCreate(Bundle savedInstanceState) {
    }

    public void onCreateView(Bundle savedInstanceState) {
    }

    public void onStart() {
    }

    public void onResume() {
    }

    public void onPause() {
    }

    public void onStop() {
    }

    public void onDestroy() {
    }

    public void onSaveInstanceState(Bundle outState) {
    }
}
