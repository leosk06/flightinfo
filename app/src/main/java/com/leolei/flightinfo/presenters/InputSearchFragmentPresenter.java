package com.leolei.flightinfo.presenters;

import java.lang.ref.WeakReference;
import java.util.Calendar;

import android.os.Bundle;
import android.text.format.DateUtils;

import com.leolei.flightinfo.Application;

/**
 * Created by leolei on 2016/2/14.
 */

public class InputSearchFragmentPresenter extends Presenter {
    private static final String CALENDAR_TIME_MILLIS = "calendarTimeMillis";
    private static final String CARRIER_CODE = "carrierCode";
    private static final String FLIGHT_NUMBER = "flightNumber";

    private WeakReference<IView> viewReference;
    private Calendar calendar;
    private String carrierCode = "";
    private String flightNumber = "";
    private boolean validInput = false;

    public InputSearchFragmentPresenter(IView view) {
        this.viewReference = new WeakReference<>(view);
        calendar = Calendar.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            calendar.setTimeInMillis(savedInstanceState.getLong(CALENDAR_TIME_MILLIS));
            carrierCode = savedInstanceState.getString(CARRIER_CODE);
            flightNumber = savedInstanceState.getString(FLIGHT_NUMBER);
        }
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }
        view.setDate(getFormattedDateString());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(CALENDAR_TIME_MILLIS, calendar.getTimeInMillis());
        outState.putString(CARRIER_CODE, carrierCode);
        outState.putString(FLIGHT_NUMBER, flightNumber);
    }

    // Search button
    public void onSearchClicked() {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        view.setCarrierCodeErrorState(carrierCode.isEmpty());
        view.setFlightNumberErrorState(flightNumber.isEmpty());

        if (!validInput) {
            view.showInvalidInputErrorMessage();
        } else {
            view.notifySearchClicked(carrierCode, flightNumber, calendar.getTimeInMillis());
        }
    }

    // Calendar
    public void onDateSet(int year, int monthOfYear, int dayOfMonth) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        view.setDate(getFormattedDateString());
    }

    private String getFormattedDateString() {
        return DateUtils.formatDateTime(Application.getInstance(), calendar.getTimeInMillis(),
                DateUtils.FORMAT_NUMERIC_DATE);
    }

    public int getYear() {
        return calendar.get(Calendar.YEAR);
    }

    public int getMonth() {
        return calendar.get(Calendar.MONTH);
    }

    public int getDay() {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public void onCarrierCodeChanged(String newCarrierCode) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        carrierCode = newCarrierCode;
        validateInput();
        view.setCarrierCodeErrorState(carrierCode.isEmpty());

    }

    public void onFlightNumberChanged(String newFlightNumber) {
        IView view = viewReference.get();
        if (view == null) {
            return;
        }

        flightNumber = newFlightNumber;
        validateInput();
        view.setFlightNumberErrorState(flightNumber.isEmpty());
    }

    private void validateInput() {
        validInput = !(carrierCode.isEmpty() || flightNumber.isEmpty());
    }

    public interface IView {
        void setDate(String date);

        void setCarrierCodeErrorState(boolean b);

        void setFlightNumberErrorState(boolean b);

        void showInvalidInputErrorMessage();

        void notifySearchClicked(String carrierCode, String flightNumber, long dateInMillis);
    }
}
