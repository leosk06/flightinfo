package com.leolei.flightinfo.model;

/**
 * Created by leolei on 2016/2/16.
 */

public class Airline {
    private String fs;
    private String name;

    public String getFsCode() {
        return fs;
    }

    public String getName() {
        return name;
    }
}
