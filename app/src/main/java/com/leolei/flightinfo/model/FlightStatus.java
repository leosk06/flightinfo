package com.leolei.flightinfo.model;

/**
 * Created by leolei on 2016/2/16.
 */

public class FlightStatus {
    private String carrierFsCode;
    private String flightNumber;
    private String departureAirportFsCode;
    private String arrivalAirportFsCode;
    private Date arrivalDate;
    private Date departureDate;

    private Airline airline;
    private Airport departureAirport;
    private Airport arrivalAirport;



    public String getAirlineFsCode() {
        return carrierFsCode;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getDepartureAirportFsCode() {
        return departureAirportFsCode;
    }

    public String getArrivalAirportFsCode() {
        return arrivalAirportFsCode;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public Airport getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(Airport departureAirport) {
        this.departureAirport = departureAirport;
    }

    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(Airport arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }
}
