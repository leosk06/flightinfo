package com.leolei.flightinfo.model;

/**
 * Created by leolei on 2016/2/16.
 */

public class ErrorMessage {
    private String errorCode;
    private String errorMessage;

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
