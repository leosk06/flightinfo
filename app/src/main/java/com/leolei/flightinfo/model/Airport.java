package com.leolei.flightinfo.model;

/**
 * Created by leolei on 2016/2/16.
 */

public class Airport {
    private String fs;
    private String name;
    private String city;
    private String cityCode;
    private String countryName;
    private float utcOffsetHours;

    public String getFsCode() {
        return fs;
    }

    public String getName() {
        return name;
    }

    public String getCityName() {
        return city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public float getUtcOffsetHours() {
        return utcOffsetHours;
    }
}
