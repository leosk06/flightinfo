package com.leolei.flightinfo.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.leolei.flightinfo.model.Airline;
import com.leolei.flightinfo.model.Airport;
import com.leolei.flightinfo.model.FlightStatus;

/**
 * Created by leolei on 2016/2/16.
 */

public class Appendix {
    private List<Airline> airlines;
    private List<Airport> airports;
    private List<FlightStatus> flightStatuses;

    private HashMap<String, Airline> _airlines;
    private HashMap<String, Airport> _airports;

    public void createMaps() {
        _airlines = new HashMap<>();
        if (airlines != null) {
            for (Iterator<Airline> i = airlines.iterator(); i.hasNext(); ) {
                Airline airline = i.next();
                _airlines.put(airline.getFsCode(), airline);
            }
        }
        _airports = new HashMap<>();
        if (airports != null) {
            for (Iterator<Airport> i = airports.iterator(); i.hasNext(); ) {
                Airport airport = i.next();
                _airports.put(airport.getFsCode(), airport);
            }
        }
    }

    public List<FlightStatus> getFlightStatuses() {
        return flightStatuses;
    }

    public Airline getAirline(String fsCode) {
        return _airlines.get(fsCode);
    }

    public Airport getAirport(String fsCode) {
        return _airports.get(fsCode);
    }
}
