package com.leolei.flightinfo.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.leolei.flightinfo.R;
import com.leolei.flightinfo.common.Utils;
import com.leolei.flightinfo.fragments.FlightListFragment;
import com.leolei.flightinfo.fragments.InputSearchFragment;
import com.leolei.flightinfo.presenters.MainActivityPresenter;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class MainActivity extends AppCompatActivity implements MainActivityPresenter.IView {

    private MainActivityPresenter presenter;
    private InputSearchFragment inputFragment;
    private FlightListFragment listFragment;
    private ImageView panelArrow;
    private SlidingUpPanelLayout slidingLayout;
    private boolean shouldOpenSlidingPanel = true;

    private SlidingUpPanelLayout.SimplePanelSlideListener slidingPanelSlideListener = new SlidingUpPanelLayout.SimplePanelSlideListener() {
        @Override
        public void onPanelCollapsed(View panel) {
            panelArrow.setImageResource(R.drawable.arrow_down);
            panelArrow.requestFocus();
            Utils.hideKeyboard(MainActivity.this);
        }

        @Override
        public void onPanelExpanded(View panel) {
            panelArrow.setImageResource(R.drawable.arrow_up);
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            shouldOpenSlidingPanel = false;
        }

        presenter = new MainActivityPresenter(this);
        presenter.onCreate(savedInstanceState);

        FragmentManager fragmentManager = getSupportFragmentManager();

        listFragment = (FlightListFragment) fragmentManager.findFragmentById(R.id.fragment_flight_list);
        inputFragment = (InputSearchFragment) fragmentManager.findFragmentById(R.id.fragment_input_search);
        inputFragment.setSearchRequestedListener(presenter);

        panelArrow = (ImageView) findViewById(R.id.panel_arrow);
        slidingLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        slidingLayout.setPanelSlideListener(slidingPanelSlideListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();

        if (shouldOpenSlidingPanel) {
            showSlidingPanel();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void hideSlidingPanel() {
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        // Invoke the listener since the component does not do this
        slidingPanelSlideListener.onPanelCollapsed(slidingLayout);
    }

    public void showSlidingPanel() {
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        slidingPanelSlideListener.onPanelExpanded(slidingLayout);
    }

    @Override
    public void refreshList() {
        listFragment.refreshList();
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        listFragment.showErrorMessage(errorMessage);
    }

    @Override
    public void showProgress() {
        listFragment.showProgressIndicator();
    }
}
