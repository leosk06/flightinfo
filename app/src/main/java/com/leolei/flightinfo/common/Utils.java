package com.leolei.flightinfo.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.leolei.flightinfo.Application;

/**
 * Created by leolei on 2016/2/15.
 */

public class Utils {
    public static void hideKeyboard(Activity activity) {
        if (activity != null) {
            final View currentFocus = activity.getCurrentFocus();
            if (currentFocus != null) {
                InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0 /* always hide */);
            }
        }
    }

    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
    public static Date networkFormatStringToDate(String s) {
        Date date = null;
        try {
            date = format.parse(s);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static String dateToLocalizedString(Date d) {
        return DateUtils.formatDateTime(Application.getInstance(), d.getTime(), DateUtils.FORMAT_ABBREV_ALL);
    }

    public static String networkFormatDateToLocalizedDate(String s) {
        return dateToLocalizedString(networkFormatStringToDate(s));
    }
}
