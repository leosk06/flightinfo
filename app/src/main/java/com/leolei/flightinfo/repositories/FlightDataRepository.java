package com.leolei.flightinfo.repositories;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.util.Log;

import com.google.gson.Gson;
import com.leolei.flightinfo.Application;
import com.leolei.flightinfo.EventBus;
import com.leolei.flightinfo.R;
import com.leolei.flightinfo.model.Appendix;
import com.leolei.flightinfo.model.ErrorMessage;
import com.leolei.flightinfo.model.FlightStatus;
import com.leolei.flightinfo.networking.FlightStatsApiResponse;
import com.leolei.flightinfo.networking.FlightStatsService;
import com.leolei.flightinfo.persistence.Storage;

/**
 * Created by leolei on 2016/2/14.
 */

public class FlightDataRepository {
    private static final String LOG_TAG = FlightDataRepository.class.getName();
    private static FlightDataRepository instance;

    private FlightStatsService service;

    // API credentials. Ideally they should never be in memory unencrypted.
    private String appId;
    private String appKey;

    private FlightStatsApiResponse lastApiResponse;

    // Save the parameters from the last call we made, so we can refresh them
    private static class FlightStatusCallParameters {
        public String carrierCode;
        public String flightNumber;
        public long dateInMillis;

        public FlightStatusCallParameters(String carrierCode, String flightNumber, long dateInMillis) {
            this.carrierCode = carrierCode;
            this.flightNumber = flightNumber;
            this.dateInMillis = dateInMillis;
        }
    }

    private FlightStatusCallParameters lastFlightStatusCallParams;

    private FlightDataRepository() {
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Application.getInstance().getString(R.string.base_url)).build();
        service = retrofit.create(FlightStatsService.class);

        appId = Application.getInstance().getString(R.string.webservice_appId);
        appKey = Application.getInstance().getString(R.string.webservice_appKey);
    }

    public static FlightDataRepository getInstance() {
        if (instance == null) {
            instance = new FlightDataRepository();
        }
        return instance;
    }

    private final Callback<FlightStatsApiResponse> flightInfoCallback = new Callback<FlightStatsApiResponse>() {
        @Override
        public void onResponse(Call<FlightStatsApiResponse> call, Response<FlightStatsApiResponse> response) {
            FlightStatsApiResponse apiResponse = response.body();

            // Check for application-level errors
            ErrorMessage errorMessage = apiResponse.getError();
            if (errorMessage != null) {
                EventBus.getInstance().post(errorMessage);
                return;
            }

            // We have a "good" result. If it is not empty, persist it
            Appendix appendix = apiResponse.getAppendix();
            appendix.createMaps();
            List<FlightStatus> flightStatuses = apiResponse.getFlightStatuses();

            for (FlightStatus fs : flightStatuses) {
                fs.setDepartureAirport(appendix.getAirport(fs.getDepartureAirportFsCode()));
                fs.setArrivalAirport(appendix.getAirport(fs.getArrivalAirportFsCode()));
                fs.setAirline(appendix.getAirline(fs.getAirlineFsCode()));
            }

            if (flightStatuses.size() > 0) {
                lastApiResponse = apiResponse;
                Storage.getInstance().persistLastResult(apiResponse);
            }

            EventBus.getInstance().post(apiResponse);
        }

        @Override
        public void onFailure(Call<FlightStatsApiResponse> call, Throwable t) {
            Log.e(LOG_TAG, new Gson().toJson(t));
            // An connectivity error occurred, so show our last good result, if we have one.
            lastApiResponse = Storage.getInstance().getLastResult();
        }
    };

    public void runFlightStatusRequest(String carrierCode, String flightNumber, long dateInMillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(dateInMillis);

        lastFlightStatusCallParams = new FlightStatusCallParameters(carrierCode, flightNumber, dateInMillis);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1; /* +1 because Calendar returns zero-based months */
        int day = cal.get(Calendar.DAY_OF_MONTH);

        Call<FlightStatsApiResponse> call = service.getFlightInfoByDepartureDate(carrierCode, flightNumber, year,
                month, day, appId, appKey);
        call.enqueue(flightInfoCallback);
    }

    private void runFlightStatusRequest(FlightStatusCallParameters params) {
        runFlightStatusRequest(params.carrierCode, params.flightNumber, params.dateInMillis);
    }

    /**
     * Repeats the last Flight status request.
     * @return true if there was one such call to repeat, false otherwise
     */
    public boolean repeatLastFlightStatusRequest() {
        if (lastFlightStatusCallParams != null) {
            runFlightStatusRequest(lastFlightStatusCallParams);
            return true;
        }
        return false;
    }

    public FlightStatsApiResponse getLastApiResponse() {
        return lastApiResponse;
    }
}
