package com.leolei.flightinfo.persistence;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.leolei.flightinfo.Application;
import com.leolei.flightinfo.networking.FlightStatsApiResponse;

/**
 * Created by leolei on 2016/2/16.
 */

public class Storage {
    private static final String STORAGE = Storage.class.getName();
    private static final String KEY_LAST_RESULT = "lastResult";

    private static Storage instance;
    private final Gson gson;

    private SharedPreferences sharedPrefs;

    private Storage() {
        sharedPrefs = Application.getInstance().getSharedPreferences(STORAGE, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }

    public void persistLastResult(FlightStatsApiResponse value) {
        sharedPrefs.edit().putString(KEY_LAST_RESULT, gson.toJson(value));
    }

    public FlightStatsApiResponse getLastResult() {
        if (sharedPrefs.contains(KEY_LAST_RESULT)) {
            String json = sharedPrefs.getString(KEY_LAST_RESULT, null);
            if (json != null) {
                return gson.fromJson(json, FlightStatsApiResponse.class);
            }
        }
        return null;
    }
}
