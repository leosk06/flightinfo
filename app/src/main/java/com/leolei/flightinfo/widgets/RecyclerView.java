package com.leolei.flightinfo.widgets;

import android.content.Context;
import android.util.AttributeSet;

/**
 *
 * Fixes a RecyclerView bug that does not allow to use it together with SwipeRefreshLayout
 * Originally from http://stackoverflow.com/a/25227797/545026
 *
 * Created by leolei on 2016/2/16.
 */

public class RecyclerView extends android.support.v7.widget.RecyclerView {
    public RecyclerView(Context context) {
        super(context);
    }

    public RecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean canScrollVertically(int direction) {
        // check if scrolling up
        if (direction < 1) {
            boolean original = super.canScrollVertically(direction);
            return !original && getChildAt(0) != null && getChildAt(0).getTop() < 0 || original;
        }
        return super.canScrollVertically(direction);

    }
}
