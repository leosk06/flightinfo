package com.leolei.flightinfo.widgets;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.rengwuxian.materialedittext.MaterialEditText;

/**
 *  This is a thin veneer over EditText, with copy/paste/spell-check removed.
 *  Credit to user CJBS at http://stackoverflow.com/a/28893714/545026
 */
public class MaterialNoMenuNotEditableEditText extends MaterialEditText {

    /** This is a replacement method for the base TextView class' method of the same name. This 
     * method is used in hidden class android.widget.Editor to determine whether the PASTE/REPLACE popup
     * appears when triggered from the text insertion handle. Returning false forces this window
     * to never appear.
     * @return false
     */
    boolean canPaste() {
        return false;
    }

    /** This is a replacement method for the base TextView class' method of the same name. This method
     * is used in hidden class android.widget.Editor to determine whether the PASTE/REPLACE popup
     * appears when triggered from the text insertion handle. Returning false forces this window
     * to never appear.
     * @return false
     */
    @Override
    public boolean isSuggestionsEnabled() {
        return false;
    }

    public MaterialNoMenuNotEditableEditText(Context context) {
        super(context);
        init();
    }

    public MaterialNoMenuNotEditableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MaterialNoMenuNotEditableEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setInputType(InputType.TYPE_NULL);
        if (android.os.Build.VERSION.SDK_INT < 11) {
            setOnCreateContextMenuListener(new OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.clear();
                }
            });
        } else {
            /**
             * Prevents the action bar (top horizontal bar with cut, copy, paste, etc.) from appearing
             * by intercepting the callback that would cause it to be created, and returning false.
             */
            this.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                public void onDestroyActionMode(ActionMode mode) {
                }
            });
        }
        this.setLongClickable(false);
    }
}
